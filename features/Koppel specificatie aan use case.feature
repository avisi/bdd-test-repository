Functionaliteit: Koppel een specificatie aan meerdere use cases
    
    #uC1.10e
	Gegeven dat use case "1.1" bestaat in de space "BDD"
	Als ik een nieuwe specificatie toevoeg met de volgende inhoud "#UC1.1"
	Dan verwacht ik dat er een label "UC1.1" wordt toegevoegd aan de "specificatie"